from django.db import models

class Users(models.Model):
    name = models.CharField(max_length=20)
    age = models.IntegerField()
    gender = models.BooleanField()

    def __str__(self):
        return 'Name: {}\tAge: {}\tGender {}'.format(self.name, self.age, 'М' if self.gender else 'Ж')

    def get_gender(self):
        return 'М' if self.gender else 'Ж'