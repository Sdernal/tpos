from django.shortcuts import render
from myapp.models import Users
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html', {'data' : 'Здесь написана очень полезная информация (нет)'})

def users(request):
    return render(request, 'users.html', {'users' : Users.objects.all().order_by("name")})