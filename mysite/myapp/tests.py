from django.test import TestCase
from .models import Users

class UsersTest(TestCase):

    def setUp(self):
        Users.objects.create(name='Alice', age=18, gender=False)
        Users.objects.create(name='Bruce', age=21, gender=True)

    def create_user(self, name, age, gender):
        return Users.objects.create(name=name, age=age, gender=gender)

    def test_users_creattion(self):
        user = self.create_user('Petya', 18, True)
        self.assertTrue(isinstance(user, Users))
        self.assertEqual(user.get_gender(), 'М')

    def test_alice(self):
        alice = Users.objects.get(name='Alice')
        self.assertTrue(isinstance(alice, Users))
        self.assertEqual(alice.age, 18)

    def test_bruce(self):
        bruce = Users.objects.get(name='Bruce')
        self.assertTrue(isinstance(bruce, Users))
        self.assertEqual(bruce.gender, True)

    def test_count(self):
        users_count = Users.objects.count()
        self.assertEqual(users_count, 2)
