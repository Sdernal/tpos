from setuptools import find_packages, setup

with open("README", 'r') as f:
    long_description = f.read()

setup(
    name='mysite',
    version='1.0',
    description='mysite',
    long_description = long_description,
    author='Sdernal',
    author_email='sdernal2@gmail.com',
    packages=['mysite', 'myapp'],
    scripts = ['manage.py'],
    install_requires=['django']
)